# Ryan McGrail
# p1.s
    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
	# int score = 84;
	addi $s0, $0, 84	# s0 = 84

	# int grade;

	# if (score >= 90)
	slti $t0, $s0, 90	# t0 = (s0 < 90)
	bne $t0, $0, ElseGT80	# go to ElseGT80 if t0 != 0
	nop			# nop
	
	#	grade = 4;
	addi $s1, $0, 4		# s1 = 0 + 4
	j AfterElse		# go to AfterElse
	nop			# nop

	# else if (score >= 80)
ElseGT80:			# ElseGT80 Label
	slti $t0, $s0, 80	# t0 = (s0 < 80)
	bne $t0, $0, ElseGT70	# go to ElseGT70 if t0 != 0
	nop			# nop

	# 	grade = 3;
	addi $s1, $0, 3		# s1 = 0 + 3
	j AfterElse		# go to AfterElse
	nop			# nop

	# else if (score >= 70)
ElseGT70:			# ElseGT70 Label
	slti $t0, $s0, 70	# t0 = (s0 < 70)
	bne $t0, $0, ElseNone	# go to ElseNone if t0 != 0
	nop			# nop

	# 	grade = 2;
	addi $s1, $0, 2		# s1 = 0 + 2
	j AfterElse		# go to AfterElse
	nop			# nop

	# else
ElseNone:			# ElseNone Label

	# 	grade = 0;
	addi $s1, $0, 0		# s1 = 0 + 0
	
	# PRINT_HEX_DEC(grade);
AfterElse:			# AfterElse Label
	add $a0, $0, $s1	# a0 = 0 + s1
	ori $v0, $0, 20		# v0 = 0 | 20
	syscall			# syscall

	# EXIT;
	ori $v0, $0, 10		# v0 = 0 | 10
	syscall			# syscall
    .end main
