# Ryan McGrail
# p5.s
    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
	# (size of elt struct is 8)

	# elt *head;
	# elt *newelt;

	# newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8		# a0 = 0+8
	ori $v0, $0, 9		# v0 = 0|9
	syscall			# syscall
	add $s1, $0, $v0	# s1 = 0+v0

	# newelt->value = 1;
	addi $t0, $0, 1		# t0 = 0+1
	sw $t0, 0($s1)		# newelt->value = t0

	# newelt->next = 0;
	sw $0, 4($s1)		# newelt->next = 0

	# head = newelt;
	add $s0, $0, $s1	# s0 = 0+s1

	# newelt = (elt *) MALLOC(sizeof (elt));
	addi $a0, $0, 8		# a0 = 0+8
	ori $v0, $0, 9		# v0 = 0|9
	syscall			# syscall
	add $s1, $0, $v0	# s1 = 0+v0

	# newelt->value = 2;
	addi $t0, $0, 2		# t0 = 0+2
	sw $t0, 0($s1)		# newelt->value = t0

	# newelt->next = head;
	sw $s0, 4($s1)		# newelt->head = s0

	# head = newelt;
	add $s0, $0, $s1	# s0 = 0+s1

	# PRINT_HEX_DEC(head->value);
	lw $a0, 0($s0)		# a0 = head->value
	ori $v0, $0, 20		# v0 = 0|20
	syscall			# syscall

	# PRINT_HEX_DEC(head->next->value);
	lw $t0, 4($s0)		# t0 = head->next
	lw $a0, 0($t0)		# t0 = next->value
	ori $v0, $0, 20		# v0 = 0|20
	syscall			# syscall

	# EXIT;
        ori $v0, $0, 10		# v0 = 0|10
        syscall			# syscall
    .end main
