# Ryan McGrail
# p4.s
    .set noreorder
    .data

    .text
    .globl main
    .ent main
main:
	# record *r = (record *) MALLOC(sizeof(record));
	addi $a0, $0, 8		# a0 = 0+8
        ori $v0, $0, 9		# v0 = 0|9
        syscall			# syscall

	add $s0, $v0, $0	# s0 = v0+0

	# r->field0 = 100;
	addi $t0, $0, 100	# t0 = 0+100
	sw $t0, 0($s0)		# r->field0 = t0

	# r->field1 = -1;
	addi $t0, $0, -1	# t0 = 0+-1
	sw $t0, 4($s0)		# r->field1 = t0

	# EXIT;
        ori $v0, $0, 10     	# v0 = 0|10
        syscall			# syscall
    .end main
