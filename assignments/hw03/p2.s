# Ryan McGrail
# p2.s
    .set noreorder
    .data
	# char A[] = { 1, 25, 7, 9, -1 };
A: .byte 1, 25, 7, 9, -1		# A = {1, 25, 7, 9, -1}

    .text
    .globl main
    .ent main
main:
	# int i;
	# int current;
	# int max;

	# i = 0;
	addi $s0, $0, 0			# s0 = 0 + 0

	# current = A[0];
	la $t0, A			# t0 = &A
	lb $s1, 0($t0)			# s1 = A[0]

	# max = 0;
	addi $s2, $0, 0			# s2 = 0 + 0

	# while (current > 0) {
StartWhile:				# StartWhile Label
	slt $t1, $0, $s1		# t1 = (0 < s1)
	beq $t1, $0, SkipWhile		# go to SkipWhile if t1 == 0
	nop				# nop

	#	 if (current > max)
	slt $t1, $s2, $s1		# t1 = (s2 < s1)
	beq $t1, $0, SkipIf		# go to SkipIf if t1 == 0
	nop				# nop

	#		 max = current;
	add $s2, $0, $s1		# s2 = 0 + s1

	#	 i = i + 1;
SkipIf:					# SkipIf Label
	addi $s0, $s0, 1		# s0 = s0 + 1

	#	 current = A[i];
	add $t2, $t0, $s0		# t1 = t0 + s0
	lb $s1, 0($t2)			# s1 = A[t2]

	# }
	j StartWhile			# go to StartWhile
	nop				# nop
	
	# PRINT_HEX_DEC(max);
SkipWhile:				# SkipWhile Label
        add $a0, $0, $s2		# a0 = 0 + s2
        ori $v0, $0, 20			# v0 = 0 | 20
        syscall				# syscall

	# EXIT;
        ori $v0, $0, 10			# v0 = 0 | 10
        syscall				# syscall
    .end main
