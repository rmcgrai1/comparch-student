# Ryan McGrail
# p3.s
    .set noreorder
    .data
	# int A[8];
A: .space 32			# int A[8]

    .text
    .globl main
    .ent main
main:
	addi $t4, $0, 4		# t4 = 0+4

	# int i;

	# A[0] = 0;
	la $t0, A		# t0 = &A
	sw $0, 0($t0)		# A[0] = 0

	# A[1] = 1;
	addi $t1, $0, 1		# t1 = 0+1
	sw $t1, 4($t0)		# A[1] = t1

	# for (i = 2; i < 8; i++) {
	addi $s0, $0, 2		# s0 = 2
StartFor:
	slti $t1, $s0, 8	# t1 = (s0 < 8)
	beq $t1, $0, SkipFor	# go to SkipFor if t1 == 0
	nop			# nop
		
	#	 A[i] = A[i-1] + A[i-2];
	mult $s0, $t4		# s0 * t4
	mflo $t1		# t1 = lo

	add $t1, $t0, $t1	# t1 = t0+t1
	lw $t2, -4($t1)		# t2 = *(t1-4)
	lw $t3, -8($t1)		# t3 = *(t1-8)

	add $t2, $t2, $t3	# t2 = t2+t3
	sw $t2, 0($t1)		# A[i] = t2

	#	 PRINT_HEX_DEC(A[i]);
	add $a0, $0, $t2	# a0 = 0 + s0
        ori $v0, $0, 20     	# v0 = 0 | 20
        syscall			# syscall

	# }
	addi $s0, $s0, 1	# s0 = s0+1
	j StartFor		# go to StartFor
	nop			# nop

SkipFor:

	# EXIT;
        ori $v0, $0, 10     	# v0 = 0 | 10
        syscall			# syscall
    .end main
